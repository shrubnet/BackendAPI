<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Queue;

class ApiController extends BaseController
{
    public function taskRun( Request $request )
    {
        Queue::pushRaw( '{ "payload": "message" }' );
        return '{ "result": "success" }';
    }
}
