FROM amd64/php:7.4.2-fpm-alpine

ENV COMPOSER_VERSION=1.9.2
ENV APCU_VERSION=5.1.18
ENV USER_MAPPED=1000

# php
RUN apk add --no-cache pcre-dev ${PHPIZE_DEPS} bash shadow libzip-dev freetype-dev libpng-dev libjpeg-turbo-dev libxml2-dev autoconf g++ imagemagick-dev imagemagick libtool
RUN pecl install apcu-$APCU_VERSION xdebug imagick
RUN docker-php-ext-install pdo opcache zip pdo_mysql gd sockets
RUN docker-php-ext-enable apcu xdebug imagick
RUN apk del pcre-dev ${PHPIZE_DEPS}

# composer
RUN curl -sS https://getcomposer.org/installer | php -- \
        --filename=composer \
        --install-dir=/usr/local/bin \
		--version=${COMPOSER_VERSION} \
        && echo "alias composer='composer'" >> /root/.bashrc \
		&& mkdir -m 777 /.composer

# phpfpm
EXPOSE 9000

RUN usermod -u $USER_MAPPED www-data && groupmod -g $USER_MAPPED www-data

COPY ./www.conf /usr/local/etc/php-fpm.d/www.conf
COPY ./php.ini /usr/local/etc/php/php.ini
COPY ./php.dev.ini /usr/local/etc/php/conf.d/01-php-dev.ini

ARG MODE=dev
ENV MODE=${MODE}
RUN if [ "$MODE" = "prod" ] ; then \
        rm /usr/local/etc/php/conf.d/01-php-dev.ini /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
    ; fi

USER www-data
